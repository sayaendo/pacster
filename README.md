# Pacster #

This is Pacman clone based in Swing for graphics. As my first real project it's not a very good program by any means, with no adherence to any design patterns or correct use of Effective Java rulesets, and a messy source code, but hey... it works (with some bugs).

The maze is read from a text file and the game goes from there.

![Screen Shot 2015-03-05 at 2.00.42 PM.png](https://bitbucket.org/repo/MeBp6r/images/3879032253-Screen%20Shot%202015-03-05%20at%202.00.42%20PM.png)