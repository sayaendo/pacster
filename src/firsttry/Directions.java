package firsttry;

public class Directions {
	public static enum Direction {UP, DOWN, LEFT, RIGHT, STILL};
	boolean up=false;
	boolean down=false;
	boolean left=false;
	boolean right=false;
	
	public Directions(boolean up, boolean down, boolean left, boolean right) {
		this.up = up;
		this.down = down;
		this.left = left;
		this.right = right;
	}
	
	public void reset() {
		up = false;
		down = false;
		left = false;
		right = false;
	}
	
	public static int[] calculateMovement(Directions.Direction d) {
		int[] n = {0, 0};
		if (d == Direction.LEFT) {
			n[0] = -25;
			n[1] = 0;
			return n;
		}
		if (d == Direction.RIGHT) {
			n[0] = 25;
			n[1] = 0;
			return n;
		}
		if (d == Direction.UP) {
			n[0] = 0;
			n[1] = -25;
			return n;
		}
		if (d == Direction.DOWN) {
			n[0] = 0;
			n[1] = 25;
			return n;
		}
		return n;
	}
	
	public int[] calculateMovement() {
		int[] n = {0, 0};
		if (left) {
			n[0] = -25;
			n[1] = 0;
			return n;
		}
		if (right) {
			n[0] = 25;
			n[1] = 0;
			return n;
		}
		if (up) {
			n[0] = 0;
			n[1] = -25;
			return n;
		}
		if (down) {
			n[0] = 0;
			n[1] = 25;
			return n;
		}
		return n;
	}

}
