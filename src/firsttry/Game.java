package firsttry;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

public class Game {
	
	JFrame frame = new JFrame();
	Level level = new Level(new File("level1.txt"));
	Player player = new Player();
	GamePanel panel = new GamePanel(level, player);
	boolean isCraze = false;
	boolean isColliding;
	Directions.Direction currentDirection = Directions.Direction.STILL;
	
	
	public void buildGUI() {
		frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(480, 528);
		frame.setResizable(false);
		frame.setVisible(true);
		panel.startLevel = false;
		
		panel.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "doEnterAction");
		EnterAction enterAction = new EnterAction();
		panel.getActionMap().put("doEnterAction", enterAction);
		panel.toggleLogo = !panel.toggleLogo;
		while (true) {
			if (!panel.startLevel) {
				panel.toggleLogo = !panel.toggleLogo;
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				panel.repaint();
			} else {
				if (!level.isGhostRunning)
					level.startGhosts();
				checkCollision();
				panel.repaint();
				move(currentDirection);
			}
		}
		
	}
	
	
	
	public void startLevel() {
		panel.getActionMap().remove("doEnterAction");
		panel.startLevel = true;
		panel.getInputMap().put(KeyStroke.getKeyStroke("LEFT"), "doLeftAction");
		panel.getActionMap().put("doLeftAction", new LeftAction());
		panel.getInputMap().put(KeyStroke.getKeyStroke("RIGHT"), "doRightAction");
		panel.getActionMap().put("doRightAction", new RightAction());
		panel.getInputMap().put(KeyStroke.getKeyStroke("UP"), "doUpAction");
		panel.getActionMap().put("doUpAction", new UpAction());
		panel.getInputMap().put(KeyStroke.getKeyStroke("DOWN"), "doDownAction");
		panel.getActionMap().put("doDownAction", new DownAction());
	}
	
	public void move(Directions.Direction d) {
		if (!checkCollision(d)) {
			player.move(d);
			player.location[0]+=Directions.calculateMovement(d)[0];
			player.location[1]+=Directions.calculateMovement(d)[1];
			isFinish();
		}
		else currentDirection = Directions.Direction.STILL;
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void checkCollision() {
		for (Ghost g : level.ghosts) {
			if ((player.location[0] == g.location[0])&&(player.location[1] == g.location[1])) {
				if (isCraze)
					g.damageStatus = true;
				else {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					player.location[0] = 225;
					player.location[1] = 250;
				}
			}
		}
	}
	
	public boolean checkCollision(Directions.Direction d) {
		ArrayList<Puck> removalBuffer = new ArrayList<Puck>();
		for (Puck p : level.pucks) {
			if ((player.location[0] == p.location[0])&&(player.location[1] == p.location[1])) {
				if (p.powerStatus)
					startCraze();
			removalBuffer.add(p);
			}
		}
		if (!removalBuffer.isEmpty())
			for (Puck p : removalBuffer) {
				level.pucks.remove(p);
			}
		
		for (Ghost g : level.ghosts) {
			if ((player.location[0] == g.location[0])&&(player.location[1] == g.location[1])) {
				if (isCraze)
					g.damageStatus = true;
				else {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					player.location[0] = 225;
					player.location[1] = 250;
				}
			}
		}
		for (Wall w : level.walls) {
			if (d == Directions.Direction.LEFT) {
				if ((player.location[0]-25 == w.location[0])&&(player.location[1] == w.location[1]))
					return true;
			}
			if (d == Directions.Direction.RIGHT) {
				if ((player.location[0]+25 == w.location[0])&&(player.location[1] == w.location[1]))
					return true;
			}
			if (d == Directions.Direction.UP) {
				if ((player.location[0] == w.location[0])&&(player.location[1]-25 == w.location[1]))
					return true;
			}
			if (d == Directions.Direction.DOWN) {
				if ((player.location[0] == w.location[0])&&(player.location[1]+25 == w.location[1]))
					return true;
			}
		}
		return false;
	}
	
	public void startCraze() {
		Thread t = new Thread(new Craze());
		t.start();
	}
	
	public class Craze implements Runnable {
		public void run() {
			isCraze = true;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			isCraze = false;
		}
	}
	
	public void isFinish() {
		if (level.pucks.isEmpty())
			panel.finishLevel = true;
	}
	
	public void goLeft() {
		currentDirection = Directions.Direction.LEFT;
	}
	public void goRight() {
		currentDirection = Directions.Direction.RIGHT;
	}
	public void goUp() {
		currentDirection = Directions.Direction.UP;
	}
	public void goDown() {
		currentDirection = Directions.Direction.DOWN;
	}
	
	
	public class EnterAction extends AbstractAction {
		public void actionPerformed(ActionEvent ev) {
			startLevel();
		}
		
	}
	
	public class LeftAction extends AbstractAction {
		public void actionPerformed(ActionEvent ev) {
			goLeft();
		}
	}
	public class RightAction extends AbstractAction {
		public void actionPerformed(ActionEvent ev) {
			goRight();
		}
	}
	public class UpAction extends AbstractAction {
		public void actionPerformed(ActionEvent ev) {
			goUp();
		}
	}
	public class DownAction extends AbstractAction {
		public void actionPerformed(ActionEvent ev) {
			goDown();
		}
	}
	

}
