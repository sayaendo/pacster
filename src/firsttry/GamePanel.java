package firsttry;

import java.awt.*;

import javax.swing.*;

public class GamePanel extends JPanel {

	boolean toggleLogo = false;
	boolean startLevel = false;
	boolean finishLevel = false;
	Level level;
	Player player;
	
	public GamePanel(Level l, Player p) {
		level = l;
		player = p;
	}
	
	public void paintComponent(Graphics g) {
		if (finishLevel)
			finishScreen.paint(g, this);
		else {
			if (startLevel) {
				System.out.print("HEY");
				level.paint(g, this);
				g.drawImage(player.currentSprite, player.location[0], player.location[1], this);
			} else {
				TitleScreen.paint(g, this, toggleLogo);
			}
		}
	}

}
