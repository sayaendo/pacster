package firsttry;

import java.awt.*;
import java.util.*;

import javax.swing.*;

public class Ghost extends GameComponent {
	public static enum Type {RED, BLUE, GREEN, YELLOW};
	boolean damageStatus = false;
	Type color;
	Image blue = new ImageIcon("ghost-blue.gif").getImage();
	Image red = new ImageIcon("ghost-red.gif").getImage();
	Image green = new ImageIcon("ghost-green.gif").getImage();
	Image yellow = new ImageIcon("ghost-yellow.gif").getImage();
	Image damaged = new ImageIcon("ghost-damaged.gif").getImage();
	Directions.Direction previousDirection;
	int stuckCounter;
	
	public Ghost(int x, int y, Type c) {
		super(y, x);
		color = c;
		this.setColor(c); //** BE CAREFUL OH GOD
	}
	
	public void setColor(Type t) {
		switch (t) {
		case RED: { currentSprite = red; break; }
		case BLUE: { currentSprite = blue; break; }
		case GREEN: { currentSprite = green; break; }
		case YELLOW: { currentSprite = yellow; break; }
		}
	}
	
	public void start(ArrayList<Wall> walls) {
		Thread t = new Thread(new GhostMovement(walls));
		t.start();
	}
	
	public void AI (ArrayList<Wall> walls) {
		if (damageStatus)
			back();
		else {
			Directions d = new Directions(false, false, false, false);
			for (Wall w : walls) {
				if ((location[1]-25 == w.location[1])&&(location[0] == w.location[0]))
					d.up = true;
				if ((location[1]+25 == w.location[1])&&(location[0] == w.location[0]))
					d.down = true;
				if ((location[0]-25 == w.location[0])&&(location[1] == w.location[1]))
					d.left = true;
				if ((location[0]+25 == w.location[0])&&(location[1] == w.location[1]))
					d.right = true;
			}
			while(true) {
				int random = (int) (Math.random()*4);
				switch (random) {
				case 0: {
					if ((!d.up) && (previousDirection != Directions.Direction.DOWN)) {
						move(Directions.Direction.UP); 
						previousDirection = Directions.Direction.UP;
						return;
					}
				}
				case 1: {
					if ((!d.down) && (previousDirection != Directions.Direction.UP)) {
						move(Directions.Direction.DOWN); 
						previousDirection = Directions.Direction.DOWN;
						return;
					}
				}
				case 2: {
					if ((!d.left) && (previousDirection != Directions.Direction.RIGHT)) {
						move(Directions.Direction.LEFT); 
						previousDirection = Directions.Direction.LEFT;
						return;
					}
				}
				case 3: {
					if ((!d.right) && (previousDirection != Directions.Direction.LEFT)) {
						move(Directions.Direction.RIGHT); 
						previousDirection = Directions.Direction.RIGHT;
						return;
					}
				}
				default: break;
				}
			}
		}
	}
	
	public void move(Directions.Direction d) {
		int[] newLocation = Directions.calculateMovement(d);
		/*for (int i=0; i<newLocation[0]; i++) {
			location[0]++;
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (int i=0; i<newLocation[1]; i++) {
			location[1]++;
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}*/
		location[0]+= newLocation[0];
		location[1]+= newLocation[1];
	}
	
	public void back() {
		int[] toSpawn = {location[0]-225, location[0]-175};
		currentSprite = damaged;
		for (int i=0; i<Math.abs(toSpawn[0]); i++) {
			if (toSpawn[0] < 0)
				location[0]++;
			else location[0]--;
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (int i=0; i<Math.abs(toSpawn[1]); i++) {
			if (toSpawn[1] < 0)
				location[1]++;
			else location[1]--;
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		setColor(color);
		damageStatus = false;;
	}
	
	public class GhostMovement implements Runnable {
		ArrayList<Wall> walls;
		public GhostMovement(ArrayList<Wall> walls) {
			this.walls = walls;
		}
		public void run() {
			while(true) {
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				AI(walls);
			}
		}
	}

}
