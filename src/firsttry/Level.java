package firsttry;

import java.util.*;
import java.awt.*;
import java.io.*;

public class Level {
	ArrayList<Puck> pucks = new ArrayList<Puck>();
	ArrayList<Wall> walls = new ArrayList<Wall>();
	ArrayList<Ghost> ghosts = new ArrayList<Ghost>();
	boolean startLevel;
	boolean isGhostRunning;
	
	public Level(File f) {
		try {
			FileReader is = new FileReader(f);
			BufferedReader bs = new BufferedReader(is);
			String s; int i; int j;
			while ((s = bs.readLine()) != null) {
				String[] st = s.split("/");
				i = Integer.parseInt(st[0]);
				j = Integer.parseInt(st[1]);
				//System.out.println(i+","+j);
				walls.add(new Wall(i, j));
			}
			bs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		ghosts.add(new Ghost(175, 225, Ghost.Type.RED));
		ghosts.add(new Ghost(175, 225, Ghost.Type.BLUE));
		ghosts.add(new Ghost(175, 225, Ghost.Type.YELLOW));
		ghosts.add(new Ghost(175, 225, Ghost.Type.GREEN));
		arrangeWalls();
		arrangeMap();
	}
	
	public void startGhosts() {
		isGhostRunning = true;
		for (Ghost g : ghosts) {
			g.start(walls);
		}
	}
	public void arrangeWalls() {
		for (Wall w1 : walls) {
			Directions d = new Directions(false, false, false, false);
			for (Wall w2 : walls) {
				if (((w1.location[0]+25) == w2.location[0])&&(w1.location[1]==w2.location[1]))
					d.right = true;
				if (((w1.location[0]-25) == w2.location[0])&&(w1.location[1]==w2.location[1]))
					d.left = true;
				if (((w1.location[1]-25) == w2.location[1])&&(w1.location[0]==w2.location[0]))
					d.up = true;
				if (((w1.location[1]+25) == w2.location[1])&&(w1.location[0]==w2.location[0]))
					d.down = true;
			}
			w1.adjustSprite(d);
		}
		
	}
	
	public void arrangeMap() {
		for (int i=0; i<451; i+=25) {
			for (int j=0; j<476; j+=25) {
				if (((i == 25)&&(j == 25))||((i == 425)&&(j == 25))||((i == 25)&&(j == 450))||((i == 425)&&(j == 450)))
					pucks.add(new Puck(i, j, true));
				else {
					pucks.add(new Puck(i, j, false));
				}
			}
		}
		ArrayList<Puck> removalBuffer = new ArrayList<Puck>();
		for (Wall w : walls) {
			for (Puck p : pucks) {
				if ((p.location[0] == w.location[0])&&(p.location[1] == w.location[1]))
					removalBuffer.add(p);
			}
		}
		for (Puck p : removalBuffer) {
			pucks.remove(p);
		}
	}
	
	public void paint(Graphics g, GamePanel panel) {
		g.setColor(Color.MAGENTA);
		g.fillRect(0, 0, panel.getWidth(), panel.getHeight());
		g.setColor(Color.BLACK);
		for (Wall w : walls) {
			g.drawImage(w.currentSprite, w.location[0], w.location[1], panel);
		}
		for (Puck p : pucks) {
			g.drawImage(p.currentSprite, p.location[0], p.location[1], panel);
		}
		for (Ghost ghost : ghosts) {
			g.drawImage(ghost.currentSprite, ghost.location[0], ghost.location[1], panel);
		}
		
	}
}
