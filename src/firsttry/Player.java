package firsttry;

import java.awt.*;

import javax.swing.*;

public class Player extends GameComponent {
	Image fullSprite = new ImageIcon("pacman-closed.gif").getImage();
	Image leftSprite = new ImageIcon("pacman-open-l.gif").getImage();
	Image rightSprite = new ImageIcon("pacman-open-r.gif").getImage();
	Image downSprite = new ImageIcon("pacman-open-d.gif").getImage();
	Image upSprite = new ImageIcon("pacman-open-u.gif").getImage();
	
	public Player() {
		super(225, 250);
		currentSprite = leftSprite;
	}
	
	public void move(Directions.Direction d) {
		if (currentSprite != fullSprite)
			currentSprite = fullSprite;
		else {
			if (d == Directions.Direction.DOWN)
				currentSprite = downSprite;
			if (d == Directions.Direction.LEFT)
				currentSprite = leftSprite;
			if (d == Directions.Direction.RIGHT)
				currentSprite = rightSprite;
			if (d == Directions.Direction.UP)
				currentSprite = upSprite;
		}
	}

}
