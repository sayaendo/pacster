package firsttry;

import javax.swing.ImageIcon;

public class Puck extends GameComponent {
	boolean powerStatus = false;
	
	public Puck(int x, int y, boolean stat) {
		super(x ,y);
		powerStatus = stat;
		if (!powerStatus)
			currentSprite = new ImageIcon("normalpuck.gif").getImage();
		else currentSprite = new ImageIcon("powerpuck.gif").getImage();
	}

}
