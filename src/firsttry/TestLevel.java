package firsttry;

import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class TestLevel {
	
	ArrayList<Wall> walls = new ArrayList<Wall>();
	
	public TestLevel() {
		walls.add(new Wall(75, 75));
		walls.add(new Wall(100, 75));
		walls.add(new Wall(0, 0));
		walls.add(new Wall(0, 0));
		arrangeWalls();
	}
	
	public void arrangeWalls() {
		for (Wall w1 : walls) {
			Directions d = new Directions(false, false, false, false);
			for (Wall w2 : walls) {
				if ((w1.location[0]+25) == w2.location[0])
					d.right = true;
				if ((w1.location[0]-25) == w2.location[0])
					d.left = true;
				if ((w1.location[1]-25) == w2.location[1])
					d.up = true;
				if ((w1.location[1]+25) == w2.location[1])
					d.down = true;
			}
			w1.adjustSprite(d);
		}
		
	}

}
