package firsttry;

import java.awt.Image;

import javax.swing.ImageIcon;

public class TestWall extends GameComponent {
	Image leftSprite = new ImageIcon("wall-l.gif").getImage();
	Image rightSprite = new ImageIcon("wall-r.gif").getImage();
	Image upSprite = new ImageIcon("wall-u.gif").getImage();
	Image downSprite = new ImageIcon("wall-d.gif").getImage();
	
	public TestWall(int x, int y) {
		super(x, y);
	}
	
	public void adjustSprite(Directions d) {
		if (d.down)
			currentSprite = downSprite;
		if (d.up)
			currentSprite = upSprite;
		if (d.left)
			currentSprite = leftSprite;
		if (d.right)
			currentSprite = rightSprite;
	}

}
