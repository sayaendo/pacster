package firsttry;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

public class TitleScreen {
	public static Image title = new ImageIcon("titlescreen.gif").getImage();
	
	public static void paint(Graphics g, GamePanel panel, boolean toggleLogo) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, panel.getWidth(), panel.getHeight());
	    if (toggleLogo)
	    	g.drawImage(title, 75, 75, panel);
	}
}
