package firsttry;

import java.awt.*;

import javax.swing.*;

public class Wall extends GameComponent {
	Image allSprite = new ImageIcon("wall-all.gif").getImage();
	Image leftSprite = new ImageIcon("wall-l.gif").getImage();
	Image rightSprite = new ImageIcon("wall-r.gif").getImage();
	Image upSprite = new ImageIcon("wall-u.gif").getImage();
	Image downSprite = new ImageIcon("wall-d.gif").getImage();
	Image downRightSprite = new ImageIcon("wall-dr.gif").getImage();
	Image downLeftSprite = new ImageIcon("wall-dl.gif").getImage();
	Image upRightSprite = new ImageIcon("wall-ur.gif").getImage();
	Image upLeftSprite = new ImageIcon("wall-ul.gif").getImage();
	Image horizontalSprite = new ImageIcon("wall-ud.gif").getImage();
	Image verticalSprite = new ImageIcon("wall-lr.gif").getImage();
	
	public Wall(int x, int y) {
		super(x, y);
		currentSprite = allSprite;
	}
	
	public void adjustWallTo(Wall w) {
		Directions d = new Directions(false, false, false, false);
		
	}
	
	public void adjustSprite(Directions d) {
		if (d.down && d.left) {
			currentSprite = downLeftSprite; return;
		}
		if (d.down && d.right) {
			currentSprite = downRightSprite; return;
		}
		if (d.up && d.left) {
			currentSprite = upLeftSprite; return;
		}
		if (d.up && d.right) {
			currentSprite = upRightSprite; return;
		}
		if (d.up && d.down) {
			currentSprite = horizontalSprite; return;
		}
		if (d.left && d.right) {
			currentSprite = verticalSprite; return;
		}
		if (d.down)
			currentSprite = downSprite;
		if (d.up)
			currentSprite = upSprite;
		if (d.left)
			currentSprite = leftSprite;
		if (d.right)
			currentSprite = rightSprite;
	}
}
