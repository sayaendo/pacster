package firsttry;

import java.awt.*;

import javax.swing.ImageIcon;

public class finishScreen {
	public static Image fin = new ImageIcon("fin.gif").getImage();
	
	public static void paint(Graphics g, GamePanel panel) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, panel.getWidth(), panel.getHeight());
		g.drawImage(fin, 80, 100, panel);
	}

}
