package leveleditor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.awt.event.*;

import javax.swing.*;

public class Main implements ActionListener {
	JFrame frame = new JFrame();
	JPanel panel = new JPanel();
	JCheckBox[][] check = new JCheckBox[19][20];
	JButton button = new JButton("save");
	
	public static void main(String[] args) {
		new Main().go1();
	}
	
	public void go1() {
		frame.add(panel);
		for (int i=0; i<check.length; i++) {
			for (int j=0; j<check[i].length; j++) {
				check[i][j] = new JCheckBox();
				panel.add(check[i][j]);
			}
		}
		button.addActionListener(new Main());
		panel.add(button);
		frame.setSize(600,600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public void go(JCheckBox[][] c) {
		File f = new File("level2.txt");
		try {
			PrintWriter w = new PrintWriter(f);
			for (int i=0; i<c.length; i++)
				for (int j=0; j<c[i].length; j++) {
					if (c[i][j].isSelected()) {
						String s = i + "/" + j;
						w.write(s);
					}
				}
			w.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void actionPerformed(ActionEvent ev) {
		go(check);
	}

}
