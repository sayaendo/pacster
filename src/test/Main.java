package test;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.KeyStroke;



public class Main {
	JFrame frame = new JFrame();
	Panel panel = new Panel();
	
	public static void main(String[] args) {
		new Main().go();
	}
	
	public void go() {
		
		frame.add(panel);
		frame.setSize(100, 100);
		frame.setVisible(true);
		
		panel.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "doEnterAction");
		EnterAction enterAction = new EnterAction();
		panel.getActionMap().put("doEnterAction", enterAction);
		
		while(true) {
			panel.repaint();
		}
	}
	
	public void gogo() {
		panel.flag = true;
		panel.getInputMap().put(KeyStroke.getKeyStroke("LEFT"), "doLeftAction");
		panel.getActionMap().put("doLeftAction", new LeftAction());
		while(true) {
			panel.repaint();
		}
	}
	public void gogogo() {
		panel.flag = false;
	}
	
	public class EnterAction extends AbstractAction {
		public void actionPerformed(ActionEvent ev) {
			gogo();
		}
		
	}
	public class LeftAction extends AbstractAction {
		public void actionPerformed(ActionEvent ev) {
			gogogo();
		}
	}

}
